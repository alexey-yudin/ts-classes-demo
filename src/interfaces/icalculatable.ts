export interface ICalculatable<ParamsType, ReturnType> {
  plus(...collection: ParamsType[]): ReturnType;
  // minus(num: number): ReturnType;
  // minus(...collection: ParamsType[]): ReturnType;
  multiply(n: number): ReturnType;
  divide(divider: number): ReturnType;
}
