export interface IMultiply<ParamsType, ReturnType> {
  multiply(collection: ParamsType[]): ReturnType;
}
