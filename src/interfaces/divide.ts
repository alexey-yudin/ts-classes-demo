export interface IDivide<ParamsType, ReturnType> {
  divide(collection: ParamsType[]): ReturnType;
}
