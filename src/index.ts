import {IntBuilder} from "./IntBuilder";
import {StringBuilder} from "./StringBuilder";

const intBuilder = new IntBuilder(5);

const result = intBuilder
  .plus(2, 2)
  .minus(1, 1)
  .multiply(2)
  .divide(2)
  .mod(2)
  .get();

// console.log(result); // 14

const strBuilder = new StringBuilder('Hello');

const strResult = strBuilder
  .plus(' all', '!')                         // 'Hello all!'
  .minus(4)                                  // 'Hello '
  .multiply(3)                               // 'Hello Hello Hello '
  .divide(4)                                 // 'Hell';
  // .remove('l')                               // 'He';
  // .sub(1,1)                                  // 'e';
  .get();

console.log(strResult);
