import {BaseBuilder} from "./BaseBuilder";
import {ICalculatable} from "./interfaces/icalculatable";

export class StringBuilder extends BaseBuilder<string> implements ICalculatable<string, StringBuilder>{
  constructor(str: string) {
    super(str);
  }

  plus(...items: string[]): StringBuilder {
    items.forEach(item => {
      this.n += item;
    });

    return this;
  }

  minus(n: number): any {
    this.n = this.n.split('').splice(0, this.n.length - n).join('');

    return this;
  }

  multiply(n: number): StringBuilder {
    let str = '';

    for (let i = 0; i < n; i++) {
      str += `${this.n} `;
    }

    this.n = str;

    return this;
  }

  divide(n: number): StringBuilder {
    const k = Math.floor(this.n.length / n);

    this.n = this.n.slice(k);

    return this;
  }

  remove(str: string): StringBuilder {
    return this;
  }

  sub(from: number, n: number): StringBuilder {
    return this;
  }
}
