export abstract class BaseBuilder<NType> {
  protected n: NType;

  protected constructor(n: NType) {
    this.n = n;
  }

  get() {
    return this.n;
  }
}
