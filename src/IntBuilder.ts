import {BaseBuilder} from "./BaseBuilder";
import {ICalculatable} from "./interfaces/icalculatable";

export class IntBuilder extends BaseBuilder<number> implements ICalculatable<number, IntBuilder>{
  constructor(num: number) {
    super(num);
  }

  plus(...numbers: number[]): IntBuilder {
    numbers.forEach(num => {
      this.n += num;
    });

    return this;
  }

  minus(...numbers: number[] | string[]): IntBuilder {
    numbers.forEach(num => {
      this.n -= num;
    });

    return this;
  }

  multiply(n: number): IntBuilder {
    this.n *= n;

    return this;
  }

  divide(n: number): IntBuilder {
    this.n = parseInt((this.n / n).toFixed(0));

    return this;
  }

  mod(mod: number): IntBuilder {
    this.n = this.n % mod;

    return this;
  }
}
